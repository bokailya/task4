using System.Linq;

namespace BitContainer
{
    public class BitContainer : System.Collections.Generic.IEnumerable<bool>
    {
        public int Length { get; private set; } = 0;

        System.Collections.IEnumerator
        System.Collections.IEnumerable.GetEnumerator()
        {
            return new Enumerator(this);
        }


        public System.Collections.Generic.IEnumerator<bool> GetEnumerator()
        {
            return new Enumerator(this);
        }


        public bool getBit(int index)
        {
            RangeCheck(index);
            return (data[index / intBits] & 1 << index % intBits) != 0;
        }


        public bool this[int index]
        {
            get { return getBit(index); }
            set { setBit(index, value); }
        }


        public override string ToString()
        {
            return
            string.Join("", this.Select(bit => System.Convert.ToInt32(bit)));
        }


        public void Clear()
        {
            data.Clear();
            Length = 0;
        }


        public void Insert(int index, bool bit)
        {
            int GetLastBit(int value)
            {
                return (value & (1 << (intBits - 1))) != 0 ? 1 : 0;
            }


            if (index < 0 || index > Length)
                throw new System.IndexOutOfRangeException();
            int
            inner_index = index % intBits,
            outer_index = index / intBits,
            tail = ~((1 << inner_index) - 1) & data[outer_index];
            data[outer_index]
            = data[outer_index] ^ tail
            | System.Convert.ToInt32(bit) << inner_index
            | tail << 1;
            if (++Length % intBits == 1)
                data.Add(0);
            int prev_last_bit = GetLastBit(tail);
            while ((Length - 1) / intBits >= ++outer_index)
            {
                int last_bit
                = GetLastBit(data[outer_index]);
                data[outer_index] = data[outer_index] << 1 | prev_last_bit;
                prev_last_bit = last_bit;
            }
        }


        public void Remove(int index)
        {
            int GetFirstBit(int value)
            {
                return value & 1;
            }


            RangeCheck(index);
            int
            inner_index = index % intBits,
            outer_index = index / intBits,
            tail = ~((1 << inner_index) - 1) & data[outer_index];
            data[outer_index]
            = data[outer_index] ^ tail
            | tail >> (inner_index + 1) << inner_index;
            while ((Length - 1) / intBits > outer_index)
            {
                data[outer_index]
                |= GetFirstBit(data[outer_index + 1]) << (intBits - 1);
                data[++outer_index] >>= 1;
            }
            if (--Length % intBits == 0)
                data.RemoveAt(data.Count - 1);
        }


        public BitContainer pushBit(bool bit)
        {
            if (++Length % intBits == 1)
                data.Add(0);
            data[(Length - 1) / intBits]
            |= System.Convert.ToInt32(bit) << (Length - 1) % intBits;
            return this;
        }


        public BitContainer pushBit(int bit)
        {
            if (bit != 0 && bit != 1)
                throw new System.ArgumentException("pushBit requires 0 or 1");
            if (++Length % intBits == 1)
                data.Add(0);
            data[(Length - 1) / intBits] |= bit << (Length - 1) % intBits;
            return this;
        }


        public void setBit(int index, bool bit)
        {
            RangeCheck(index);
            int outer_index = index / intBits, inner_index = index % intBits;
            if ((data[outer_index] & (1 << inner_index)) != 0 != bit)
                data[outer_index] ^= 1  << inner_index;
        }


        private class Enumerator
        : System.Collections.Generic.IEnumerator<bool>
        {
            readonly BitContainer bitContainer;

            object System.Collections.IEnumerator.Current
            {
                get
                {
                    return
                    (bitContainer.data[index / intBits] & 1 << index % intBits)
                    != 0;
                }
            }


            public Enumerator(BitContainer container)
            {
                bitContainer = container;
            }


            public bool Current
            {
                get
                {
                    return
                   (bitContainer.data[index / intBits] & 1 << index % intBits)
                   != 0;
                }
            }


            public bool MoveNext()
            {
                return ++index < bitContainer.Length;
            }


            public void Dispose()
            {
            }


            public void Reset()
            {
                index = -1;
            }

            private int index = -1;
        }

        private System.Collections.Generic.List<int> data
        = new System.Collections.Generic.List<int>();
		private const int intBits = sizeof(int) * 8;

        private void RangeCheck(int index)
        {
            if (index < 0 || index >= Length)
                throw new System.IndexOutOfRangeException();
        }
    }
}
