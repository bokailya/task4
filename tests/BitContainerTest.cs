using NUnit.Framework;

namespace Tests
{
    public class BitContainerTest
    {
        [Test]
        public void Length_NewContainer_LengthIsZero()
        {
            Assert.That(new BitContainer.BitContainer().Length, Is.EqualTo(0));
        }


        [Test]
        public void Length_NewContainerThreeFalsePushed_LengthIsThree()
        {
            Assert.That(
                    new
                    BitContainer.BitContainer()
                    .pushBit(false).pushBit(false).pushBit(false).Length,
                    Is.EqualTo(3));
        }


        [Test]
        public void
            Length_FortyEightElementsContainerFalsePushed_LengthIsThree()
            {
                Assert.That(
                        new
                        BitContainer.BitContainer()
                        .pushBit(false).pushBit(false).pushBit(false)
                        .pushBit(false).pushBit(false).pushBit(false)
                        .pushBit(false).pushBit(false).pushBit(false)
                        .pushBit(false).pushBit(false).pushBit(false)
                        .pushBit(false).pushBit(false).pushBit(false)
                        .pushBit(false).pushBit(false).pushBit(false)
                        .pushBit(false).pushBit(false).pushBit(false)
                        .pushBit(false).pushBit(false).pushBit(false)
                        .pushBit(false).pushBit(false).pushBit(false)
                        .pushBit(false).pushBit(false).pushBit(false)
                        .pushBit(false).pushBit(false).pushBit(false)
                        .pushBit(false).pushBit(false).pushBit(false)
                        .pushBit(false).pushBit(false).pushBit(false)
                        .pushBit(false).pushBit(false).pushBit(false)
                        .pushBit(false).pushBit(false).pushBit(false)
                        .pushBit(false).pushBit(false).pushBit(false)
                        .Length,
                        Is.EqualTo(48));
            }


        [Test]
        public void getBit_FiveElementsContainerGetMinusOne_ThrowsException()
        {
            Assert.That(
                    () =>
                    new BitContainer.BitContainer()
                    .pushBit(true).pushBit(false).pushBit(true).pushBit(true)
                    .pushBit(false).getBit(-1),
                    Throws.TypeOf<System.IndexOutOfRangeException>());
        }


        [Test]
        public void getBit_FiveElementsContainerGetThird_ReturnsTrue()
        {
            Assert.That(
                    new
                    BitContainer.BitContainer()
                    .pushBit(false).pushBit(false)
                    .pushBit(true)
                    .pushBit(false).pushBit(false)
                    .getBit(2),
                    Is.True);
        }


        [Test]
        public void
            getBit_FortyEightElementsContainerGetThirtyThird_ReturnsFalse()
            {
                Assert.That(
                        new
                        BitContainer.BitContainer()
                        .pushBit(true).pushBit(true).pushBit(true).pushBit(true)
                        .pushBit(true).pushBit(true).pushBit(true).pushBit(true)
                        .pushBit(true).pushBit(true).pushBit(true).pushBit(true)
                        .pushBit(true).pushBit(true).pushBit(true).pushBit(true)
                        .pushBit(true).pushBit(true).pushBit(true).pushBit(true)
                        .pushBit(true).pushBit(true).pushBit(true).pushBit(true)
                        .pushBit(true).pushBit(true).pushBit(true).pushBit(true)
                        .pushBit(true).pushBit(true).pushBit(true).pushBit(true)
                        .pushBit(false).pushBit(true).pushBit(true)
                        .pushBit(true).pushBit(true).pushBit(true).pushBit(true)
                        .pushBit(true).pushBit(true).pushBit(true).pushBit(true)
                        .pushBit(true).pushBit(true).pushBit(true).pushBit(true)
                        .pushBit(true)
                        .getBit(32),
                        Is.False);
            }


        [Test]
        public void
            squareBracketsGet_ThreeElementsContainerGetFourth_ThrowsException()
        {
            Assert.That(
                    () =>
                    new BitContainer.BitContainer()
                    .pushBit(true).pushBit(false).pushBit(true)[4],
                    Throws.TypeOf<System.IndexOutOfRangeException>());
        }


        [Test]
        public void
            squareBracketsGet_ThreeElementsContainerGetFirst_ReturnsFalse()
            {
                BitContainer.BitContainer bitContainer
                    =
                    new
                    BitContainer.BitContainer()
                    .pushBit(false).pushBit(true).pushBit(true);
                Assert.That(bitContainer[0], Is.False);
            }


        [Test]
        public void
            squareBracketsGet_FortySixContainerGetThirtyFive_ReturnsTrue()
            {
                BitContainer.BitContainer bitContainer
                    =
                    new
                    BitContainer.BitContainer()
                    .pushBit(true).pushBit(true).pushBit(true).pushBit(true)
                    .pushBit(true).pushBit(true).pushBit(true).pushBit(true)
                    .pushBit(true).pushBit(true).pushBit(true).pushBit(true)
                    .pushBit(true).pushBit(true).pushBit(true).pushBit(true)
                    .pushBit(true).pushBit(true).pushBit(true).pushBit(true)
                    .pushBit(true).pushBit(true).pushBit(true).pushBit(true)
                    .pushBit(true).pushBit(true).pushBit(true).pushBit(true)
                    .pushBit(true).pushBit(true).pushBit(true).pushBit(true)
                    .pushBit(true).pushBit(true).pushBit(true).pushBit(true)
                    .pushBit(true).pushBit(true).pushBit(true).pushBit(true)
                    .pushBit(true).pushBit(true).pushBit(true).pushBit(true)
                    .pushBit(true).pushBit(true);
                Assert.That(bitContainer[34], Is.True);
            }


        [Test]
        public void
            squareBracketsSet_FourElementsSetMinusSecond_ThrowsException()
        {
            Assert.That(
                    () =>
                    new BitContainer.BitContainer()
                    .pushBit(true).pushBit(false).pushBit(false).pushBit(false)
                    [-2] = false,
                    Throws.TypeOf<System.IndexOutOfRangeException>());
        }


        [Test]
        public void squareBracketsSet_FourElementsContainerSetSecondToTrue()
        {
            BitContainer.BitContainer bitContainer
                =
                new
                BitContainer.BitContainer()
                .pushBit(false).pushBit(false).pushBit(false).pushBit(false);
            bitContainer[1] = true;
            Assert.That(bitContainer.ToString(), Is.EqualTo("0100"));
        }


        [Test]
        public void squareBracketsSet_FortySevenContainerSetThirtyFourToFalse()
        {
            BitContainer.BitContainer bitContainer
                =
                new
                BitContainer.BitContainer()
                .pushBit(false).pushBit(false).pushBit(false).pushBit(false)
                .pushBit(false).pushBit(false).pushBit(false).pushBit(false)
                .pushBit(false).pushBit(false).pushBit(false).pushBit(false)
                .pushBit(false).pushBit(false).pushBit(false).pushBit(false)
                .pushBit(false).pushBit(false).pushBit(false).pushBit(false)
                .pushBit(false).pushBit(false).pushBit(false).pushBit(false)
                .pushBit(false).pushBit(false).pushBit(false).pushBit(false)
                .pushBit(false).pushBit(false).pushBit(false).pushBit(false)
                .pushBit(false).pushBit(false).pushBit(false).pushBit(false)
                .pushBit(false).pushBit(false).pushBit(false).pushBit(false)
                .pushBit(false).pushBit(false).pushBit(false).pushBit(false)
                .pushBit(false).pushBit(false).pushBit(false);
            bitContainer[33] = true;
            Assert.That(
                    bitContainer.ToString(),
                    Is.EqualTo(
                        "00000000000000000000000000000000010000000000000"));
        }


        [Test]
        public void ToString_TwoElementsContainerTrueFalse_ResultString10()
        {
            Assert.That(
                    new
                    BitContainer.BitContainer()
                    .pushBit(true).pushBit(false).ToString(),
                    Is.EqualTo("10"));
        }


        [Test]
        public void ToString_FortyFiveElementsContainer()
        {
            Assert.That(
                    new
                    BitContainer.BitContainer()
                    .pushBit(true).pushBit(false).pushBit(false).pushBit(false)
                    .pushBit(false).pushBit(true).pushBit(true).pushBit(false)
                    .pushBit(true).pushBit(true).pushBit(false).pushBit(false)
                    .pushBit(false).pushBit(true).pushBit(false).pushBit(true)
                    .pushBit(true).pushBit(false).pushBit(true).pushBit(false)
                    .pushBit(false).pushBit(false).pushBit(true).pushBit(false)
                    .pushBit(true).pushBit(true).pushBit(false).pushBit(false)
                    .pushBit(true).pushBit(true).pushBit(true).pushBit(false)
                    .pushBit(true).pushBit(false).pushBit(false).pushBit(true)
                    .pushBit(false).pushBit(false).pushBit(false).pushBit(false)
                    .pushBit(true).pushBit(true).pushBit(true).pushBit(false)
                    .pushBit(true).pushBit(false)
                    .ToString(),
                    Is.EqualTo(
                        "1000011011000101101000101100111010010000111010"));
        }


        [Test]
        public void Clear_OneElementContainer_LengthIsZero_ToStringEmpty()
        {
            BitContainer.BitContainer bitContainer
                = new BitContainer.BitContainer().pushBit(false);
            bitContainer.Clear();
            Assert.That(bitContainer.Length, Is.Zero);
            Assert.That(bitContainer.ToString(), Is.Empty);
        }


        [Test]
        public void
            Clear_FortyFourElementsContainer_LengthIsZero_ToStringEmpty()
            {
                BitContainer.BitContainer bitContainer
                    = new BitContainer.BitContainer()
                    .pushBit(true).pushBit(true).pushBit(true).pushBit(false)
                    .pushBit(true).pushBit(false).pushBit(false).pushBit(true)
                    .pushBit(false).pushBit(true).pushBit(true).pushBit(false)
                    .pushBit(true).pushBit(false).pushBit(false).pushBit(false)
                    .pushBit(true).pushBit(true).pushBit(false).pushBit(false)
                    .pushBit(true).pushBit(false).pushBit(false).pushBit(false)
                    .pushBit(false).pushBit(true).pushBit(true).pushBit(true)
                    .pushBit(true).pushBit(false).pushBit(false).pushBit(false)
                    .pushBit(true).pushBit(true).pushBit(true).pushBit(false)
                    .pushBit(true).pushBit(true).pushBit(false).pushBit(true)
                    .pushBit(true).pushBit(false).pushBit(true).pushBit(true);
                bitContainer.Clear();
                Assert.That(bitContainer.Length, Is.Zero);
                Assert.That(bitContainer.ToString(), Is.Empty);
            }


        [Test]
        public void
            Insert_TwoElementsContainerInsertToFourthPosition_ThrowsException()
        {
            Assert.That(
                    () =>
                    new BitContainer.BitContainer()
                    .pushBit(true).pushBit(true).Insert(4, true),
                    Throws.TypeOf<System.IndexOutOfRangeException>());
        }


        [Test]
        public void Insert_TwoElementsContainerInsertFalseToSecondPosition()
        {
            BitContainer.BitContainer bitContainer
                = new BitContainer.BitContainer().pushBit(false).pushBit(true);
            bitContainer.Insert(1, false);
            Assert.That(bitContainer.Length, Is.EqualTo(3));
            Assert.That(bitContainer.ToString(), Is.EqualTo("001"));
        }


        [Test]
        public void
            Insert_FortyThreeElementsContainerInsertFalseToFourthPosition()
            {
                BitContainer.BitContainer bitContainer
                    = new BitContainer.BitContainer()
                    .pushBit(true).pushBit(true).pushBit(false).pushBit(false)
                    .pushBit(false).pushBit(false).pushBit(true).pushBit(true)
                    .pushBit(false).pushBit(false).pushBit(false).pushBit(false)
                    .pushBit(true).pushBit(true).pushBit(true).pushBit(true)
                    .pushBit(true).pushBit(false).pushBit(true).pushBit(true)
                    .pushBit(true).pushBit(true).pushBit(true).pushBit(true)
                    .pushBit(false).pushBit(true).pushBit(false).pushBit(false)
                    .pushBit(false).pushBit(false).pushBit(false).pushBit(false)
                    .pushBit(false).pushBit(true).pushBit(true).pushBit(true)
                    .pushBit(true).pushBit(true).pushBit(true).pushBit(false)
                    .pushBit(true).pushBit(false).pushBit(true);
                bitContainer.Insert(3, true);
                Assert.That(bitContainer.Length, Is.EqualTo(44));
                Assert.That(
                        bitContainer.ToString(),
                        Is.EqualTo(
                            "11010001100001111101111110100000001111110101"));
            }


        [Test]
        public void
            Remove_ThreeElementsContainerRemoveMinusThird_ThrowsException()
        {
            Assert.That(
                    () =>
                    new BitContainer.BitContainer()
                    .pushBit(false).pushBit(true).pushBit(true).Remove(-3),
                    Throws.TypeOf<System.IndexOutOfRangeException>());
        }


        [Test]
        public void Remove_ThreeElementsContainerRemoveThird()
        {
            BitContainer.BitContainer bitContainer
                = new BitContainer.BitContainer()
                .pushBit(false).pushBit(true).pushBit(false);
            bitContainer.Remove(2);
            Assert.That(bitContainer.Length, Is.EqualTo(2));
            Assert.That(bitContainer.ToString(), Is.EqualTo("01"));
        }


        [Test]
        public void Remove_FortyTwoElementsContainerRemoveFifth()
        {
            BitContainer.BitContainer bitContainer
                = new BitContainer.BitContainer()
                .pushBit(true).pushBit(false).pushBit(false).pushBit(true)
                .pushBit(false).pushBit(true).pushBit(true).pushBit(false)
                .pushBit(false).pushBit(true).pushBit(true).pushBit(false)
                .pushBit(false).pushBit(false).pushBit(true).pushBit(false)
                .pushBit(true).pushBit(true).pushBit(false).pushBit(true)
                .pushBit(false).pushBit(true).pushBit(false).pushBit(true)
                .pushBit(false).pushBit(false).pushBit(true).pushBit(true)
                .pushBit(false).pushBit(true).pushBit(false).pushBit(false)
                .pushBit(false).pushBit(false).pushBit(true).pushBit(false)
                .pushBit(true).pushBit(false).pushBit(false).pushBit(true)
                .pushBit(false).pushBit(false);
            bitContainer.Remove(4);
            Assert.That(bitContainer.Length, Is.EqualTo(41));
            Assert.That(
                    bitContainer.ToString(),
                    Is.EqualTo("10011100110001011010101001101000010100100"));
        }


        [Test]
        public void pushBitBoolean_PushFourElementsToNewContainer()
        {
            BitContainer.BitContainer bitContainer
                = new BitContainer.BitContainer()
                .pushBit(false).pushBit(true).pushBit(true).pushBit(false);
            Assert.That(bitContainer.Length, Is.EqualTo(4));
            Assert.That(bitContainer.ToString(), Is.EqualTo("0110"));
        }


        [Test]
        public void pushBitBoolean_PushFourtyOneElementsToNewContainer()
        {
            BitContainer.BitContainer bitContainer
                = new BitContainer.BitContainer()
                .pushBit(true).pushBit(false).pushBit(true).pushBit(false)
                .pushBit(true).pushBit(true).pushBit(true).pushBit(false)
                .pushBit(false).pushBit(true).pushBit(false).pushBit(false)
                .pushBit(true).pushBit(true).pushBit(false).pushBit(true)
                .pushBit(true).pushBit(true).pushBit(false).pushBit(true)
                .pushBit(true).pushBit(false).pushBit(false).pushBit(false)
                .pushBit(true).pushBit(true).pushBit(true).pushBit(true)
                .pushBit(false).pushBit(true).pushBit(false).pushBit(false)
                .pushBit(false).pushBit(true).pushBit(false).pushBit(true)
                .pushBit(false).pushBit(true).pushBit(true).pushBit(false)
                .pushBit(true);
            Assert.That(bitContainer.Length, Is.EqualTo(41));
            Assert.That(
                    bitContainer.ToString(),
                    Is.EqualTo("10101110010011011101100011110100010101101"));
        }


        [Test]
        public void pushBitInt_PushFiveElementsToNewContainer()
        {
            BitContainer.BitContainer bitContainer
                = new BitContainer.BitContainer()
                .pushBit(0).pushBit(0).pushBit(0).pushBit(0).pushBit(0);
            Assert.That(bitContainer.Length, Is.EqualTo(5));
            Assert.That(bitContainer.ToString(), Is.EqualTo("00000"));
        }


        [Test]
        public void pushBitInt_PushFourtyElementsToNewContainer()
        {
            BitContainer.BitContainer bitContainer
                = new BitContainer.BitContainer()
                .pushBit(1).pushBit(0).pushBit(0).pushBit(0).pushBit(0)
                .pushBit(0).pushBit(1).pushBit(0).pushBit(1).pushBit(1)
                .pushBit(1).pushBit(0).pushBit(0).pushBit(0).pushBit(0)
                .pushBit(1).pushBit(1).pushBit(1).pushBit(0).pushBit(1)
                .pushBit(1).pushBit(1).pushBit(1).pushBit(0).pushBit(0)
                .pushBit(0).pushBit(0).pushBit(1).pushBit(0).pushBit(0)
                .pushBit(1).pushBit(0).pushBit(0).pushBit(0).pushBit(0)
                .pushBit(1).pushBit(0).pushBit(0).pushBit(0).pushBit(0);
            Assert.That(bitContainer.Length, Is.EqualTo(40));
            Assert.That(
                    bitContainer.ToString(),
                    Is.EqualTo("1000001011100001110111100001001000010000"));
        }


        [Test]
        public void Foreach_SixElementsContainer()
        {
            const int length = 6;
            System.Collections.Generic.List<bool> data
                = new System.Collections.Generic.List<bool>();
            data.Add(false);
            data.Add(false);
            data.Add(true);
            data.Add(false);
            data.Add(true);
            data.Add(true);
            BitContainer.BitContainer bitContainer
                = new BitContainer.BitContainer();
            foreach (bool element in data)
                bitContainer.pushBit(element);
            for (int i = 0; i < length; ++i)
                Assert.That(bitContainer[i], Is.EqualTo(data[i]));
        }


        [Test]
        public void Foreach_ThirtyNineElementsContainer()
        {
            const int length = 39;
            System.Collections.Generic.List<bool> data
                = new System.Collections.Generic.List<bool>();
            data.Add(true);
            data.Add(true);
            data.Add(true);
            data.Add(false);
            data.Add(true);
            data.Add(true);
            data.Add(true);
            data.Add(true);
            data.Add(true);
            data.Add(true);
            data.Add(false);
            data.Add(false);
            data.Add(true);
            data.Add(false);
            data.Add(false);
            data.Add(true);
            data.Add(true);
            data.Add(true);
            data.Add(false);
            data.Add(false);
            data.Add(false);
            data.Add(false);
            data.Add(true);
            data.Add(false);
            data.Add(false);
            data.Add(true);
            data.Add(true);
            data.Add(true);
            data.Add(false);
            data.Add(true);
            data.Add(false);
            data.Add(true);
            data.Add(true);
            data.Add(true);
            data.Add(false);
            data.Add(false);
            data.Add(false);
            data.Add(true);
            data.Add(false);

            BitContainer.BitContainer bitContainer
                = new BitContainer.BitContainer();
            foreach (bool element in data)
                bitContainer.pushBit(element);
            for (int i = 0; i < length; ++i)
                Assert.That(bitContainer[i], Is.EqualTo(data[i]));
        }
    }
}
